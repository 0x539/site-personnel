(use-modules (ice-9 i18n)
             (haunt asset)
             (haunt site)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader)
             (haunt post)
             (haunt page)
             (haunt html))

(define (disposition site titre corps)
  `((doctype "html")
    (html (@ (lang "fr-FR"))
          (head
           (meta (@ (charset "utf-8")))
           (title ,(string-append titre " — " (site-title site))))
          (body
           ,corps
           (footer
            (img (@ (src "/images/Licence_Art_Libre.svg")
                    (width "92px")
                    (height "32px")))
            (p (@ (style "margin-top:0px;"))
               "Ce site est publié selon les termes de la licence Art Libre disponible "
               (a (@ (href "http://artlibre.org")) "ici") "."))))))

(define (modèle-publication publication)
  `((h2 ,(post-ref publication 'title))
    (h3 "par " ,(post-ref publication 'author)
        " — " ,(date->string* (post-date publication)))
    (div ,(post-sxml publication))))

(define (modèle-page publication)
  `((h1 ,(post-ref publication 'title))
    (a (@ (href "/")) "Accueil")
    (div ,(post-sxml publication))))

(define (modèle-collection site titre publications préfixe)
  (define (iru-publication publication)
    (string-append (or préfixe "") "/"
                   (site-post-slug site publication) ".html"))

  `((h3 ,titre)
    (ul
     ,@(map (lambda (publication)
              `(li
                (a (@ (href ,(iru-publication publication)))
                   ,(post-ref publication 'title)
                   " — "
                   ,(date->string* (post-date publication)))))
            publications))))

(define thème-simple
  (theme #:name "Thème"
         #:layout disposition
         #:post-template modèle-page
         #:collection-template modèle-collection))

(define (obtenir-menu fichiers)
  `(ul ,@(map (lambda (fichier)
               `(li
                 (a (@ (href
                        ,(string-append fichier
                                        ".html")))
                    ,(string-upcase fichier 0 1))))
              fichiers)))

(define (créer-index thème site fichiers)
  (make-page
   "index.html"
   (with-layout thème site "Accueil"
                `((h1 "Corentin Bocquillon")
                  (p "Étudiant en troisième année de licence informatique "
                     "à Marseille.")
                  ,(obtenir-menu fichiers)
                  (p "Mon compte framagit est disponible à "
                     (a (@ (href "https://framagit.org/0x539")) "cette adresse")
                     ".")))
   sxml->html))

(define* (site-vitrine #:key (thème thème-simple))
  (lambda (site publications)
    (define (publication->page publication)
      (let ((nom (string-append (site-post-slug site publication)
                                ".html")))
        (make-page nom (render-post thème site publication)
                   sxml->html)))
    (let ((fichiers (map (lambda (publication)
                           (site-post-slug site publication))
                     publications)))
      (append (map publication->page publications)
              `(,(créer-index thème site fichiers))))))

(site #:title "Corentin Bocquillon"
      #:domain "xn--vendmiaire-e7a.fr"
      #:default-metadata
      '((author . "Corentin Bocquillon"))
      #:readers (list sxml-reader)
      #:builders (list (site-vitrine)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (static-directory "images"))
      #:posts-directory "pages")
